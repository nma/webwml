# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2019-01-18 00:06+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Hurd-CD'leri"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "İletişim"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "İşlemciler"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Katkıcılar"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Geliştirme"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Belgelendirme"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Kurulum"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Yapılandırma"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Bağlantılar"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Haberler"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Taşıma"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Taşınanlar"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problemler"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Yazılım Haritası"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Durum"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Destek"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Sistemler"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD (i386)"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD (Alpha)"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Niçin"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Kişiler"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "Dizüstü bilgisayarlar için Debian"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "Debian ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian Beowulf"

#~ msgid "Main"
#~ msgstr "Temel"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian Motorola 680x0"

#~ msgid "Vendor/Name"
#~ msgstr "Üretici/İsim"

#~ msgid "Date announced"
#~ msgstr "Duyurulduğu tarihi"

#~ msgid "Clock"
#~ msgstr "Saat"

#~ msgid "ICache"
#~ msgstr "ICache"

#~ msgid "DCache"
#~ msgstr "DCache"

#~ msgid "TLB"
#~ msgstr "TLB"

#~ msgid "ISA"
#~ msgstr "ISA"

#~ msgid "Specials"
#~ msgstr "Özel"

#~ msgid "No FPU (R2010), external caches"
#~ msgstr "FPU (R2010) yok, harici önbellek"

#~ msgid "No FPU (R3010)"
#~ msgstr "FPU (R3010)"

#~ msgid "Virtual indexed L1 cache, L2 cache controller"
#~ msgstr "Sanal endekslenmiş L1 önbellek, L2 önbellek denetleyici"

#~ msgid "External L1 cache"
#~ msgstr "Harici L1 önbellek"

#~ msgid "Multiple chip CPU"
#~ msgstr "Çoklu yongalı CPU"

#~ msgid ""
#~ "Mips16 isa extension, No FPU, 512k flash, 16k ram, DMAC, UART, Timer, "
#~ "I2C, Watchdog"
#~ msgstr ""
#~ "Mips16 isa uzantısı, FPU yok, 512k flash, 16k bellek, DMAC, UART, "
#~ "Zamanlayıcı, I2C, Watchdog"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer, I2C, LCD Controller"
#~ msgstr "FPU yok, DRAMC, ROMC, DMAC, UART, Zamanlayıcı, I2C, LCD Denetleyici"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer"
#~ msgstr "FPU yok, DRAMC, ROMC, DMAC, UART, Zamanlayıcı"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "FPU yok, DRAMC, ROMC, DMAC, PCIC, UART, Zamanlayıcı"

#~ msgid "No FPU, SDRAMC, ROMC, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr "FPU yok, SDRAMC, ROMC, Zamanlayıcı, PCMCIA, LCD Denetleyici, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr ""
#~ "FPU yok, SDRAMC, ROMC, UART, Zamanlayıcı, PCMCIA, LCD Denetleyici, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, IrDA"
#~ msgstr "FPU yok, SDRAMC, ROMC, UART, Zamanlayıcı, PCMCIA, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "FPU yok, SDRAMC, ROMC, DMAC, PCIC, UART, Zamanlayıcı"

#~ msgid "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Digital I/O"
#~ msgstr "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Sayısal I/O"

#~ msgid "FPU, 32-bit external bus"
#~ msgstr "FPU, 32-bit harici yol"

#~ msgid "FPU, 64-bit external bus"
#~ msgstr "FPU, 64-bit harici yol"

#~ msgid "FPU, 64-bit external bus, external L2 cache"
#~ msgstr "FPU, 64-bit harici yol, harici L2 önbellek"

#~ msgid "256 L2 cache on die"
#~ msgstr "Yonga üzerinde 256 L2 önbellek"

#~ msgid "Mips 16"
#~ msgstr "Mips 16"

#~ msgid ""
#~ "Mips 16, RTC, Keyboard, TouchPanel, Audio, Compact-Flash, UART, Parallel"
#~ msgstr "Mips 16, Klavye, TouchPanel, Ses, Kompakt Flash, UART, Paralel"

#~ msgid "Mips 16, Compact Flash, UART, Parallel, RTC, Audio, PCIC"
#~ msgstr "Mips 16, Kompakt Flash, UART, Paralel, RTC, Ses, PCIC"

#~ msgid ""
#~ "Mips 16, LCD controller, Compact Flash, UART, Parallel, RTC, Keyboard, "
#~ "USB, Touchpad, Audio"
#~ msgstr ""
#~ "Mips 16, LCD denetleyici, Kompakt Flash, UART, Paralel, RTC, Klavye, USB, "
#~ "Touchpad, Audio"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian MIPS"

#~ msgid "Debian for S/390"
#~ msgstr "Debian S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian Sparc64"
