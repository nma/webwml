#use wml::debian::template title="Using Git to work on the Debian Website" MAINPAGE="true"
#use wml::debian::toc

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Working on the Git Repository</a></li>
<li><a href="#write-access">Write Access to the Git Repository</a></li>
<li><a href="#notifications">Getting Notifications</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a> is a <a href="https://en.wikipedia.org/wiki/Version_control">version 
control system</a> which helps to coordinate work among multiple developers. Every user can hold a local copy of a main 
repository. The local copies can be on the same machine or across the world. 
Developers can then modify the local copy and commit their changes back to the main repository when they're ready.</p>
</aside>

<h2><a id="work-on-repository">Working on the Git Repository</a></h2>

<p>
Let's jump right into it — in this section you will learn how to create a local
copy of the main repository, how to keep that repo up-to-date, and how to submit
your work.
We'll also explain how to work on translations.
</p>

<h3><a name="get-local-repo-copy">Get a local Copy</a></h3>

<p>
First, install Git. Next, 
configure Git and enter your name and email address. 
If you're a new Git user, then it's probably a good idea to read the general Git documentation first. 
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Git Documentation</a></button></p>

<p>
Your next step is to clone the repository (in other words: make a local copy of it). 
There are two ways to do this:
</p>

<ul>
  <li>Register an account on <url https://salsa.debian.org/> and enable SSH access by uploading 
  a public SSH key to your Salsa account. See the <a 
  href="https://salsa.debian.org/help/ssh/README.md">Salsa help 
  pages</a> for more details. Then you can clone the 
  <code>webwml</code> repository using the following command:
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>As an alternative, you can 
  clone the repository using the HTTPS protocol. Keep in mind that 
  this will create the local repository, but you won't 
  be able to directly push changes back this way:
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>Tip:</strong> Cloning the entire <code>webwml</code> repository requires downloading about
1.3 GB of data which might be too much if you're on a slow or 
unstable internet connection. Therefore, it's possible to define 
a minimum depth for a smaller initial download:
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>After obtaining a usable (shallow) repository, you can deepen the
local copy and eventually convert it to a full local
repository:</p>

<pre>
  git fetch --deepen=1000 # deepen the repo for another 1000 commits
  git fetch --unshallow   # fetch all missing commits, convert the repo to a complete one
</pre>

<p>You can also check out only a subset of the pages:</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Create the file <code>.git/info/sparse-checkout</code> inside the <code>webwml</code> 
  directory to define the content you want to check out. For example, 
  if you only want to retrieve the base files, English, Catalan, and Spanish translations, 
  the file looks like this:
    <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
    </pre></li>
  <li>Finally, you can check out the repo: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Submit local Changes</a></h3>

<h4><a name="keep-local-repo-up-to-date">Keep your local Repo up-to-date</a></h4>

<p>Every few days (and definitely before starting some editing work!)
you should do a</p>

<pre>
  git pull
</pre>

<p>to retrieve any files from the repository which have changed.</p>

<p>
It is strongly recommended to keep your local Git working directory clean
before performing <code>git pull</code> and starting to edit some files.
If you have uncommitted changes or local commits that are not present in the remote
repository on the current branch, running <code>git pull</code> will automatically create
merge commits or even fail due to conflicts. Please consider keeping your
unfinished work in another branch or using commands like <code>git stash</code>.
</p>

<p>Note: Git is a distributed (not centralised) version control
system. This means that when you commit changes, they will only be
stored in your local repository. To share them with others, you will
also need to push your changes to the central repository on Salsa.</p>

<h4><a name="example-edit-english-file">Example: editing some Files</a></h4>

<p>
Let's look at a more practical example and a typical editing session. 
We're assuming that you have obtained a <a href="#get-local-repo-copy">local copy</a> of the repo 
using <code>git clone</code>. Your next steps are:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Now you can start editing and make changes to the files.</li>
  <li>When you are done, commit your changes to your local repository:
    <pre>
    git add path/to/file(s)
    git commit -m "Your commit message"
    </pre></li>
  <li>If you have <a href="#write-access">unlimited write access</a> to the remote <code>webwml</code> repository, you 
  may now push your changes directly onto the Salsa repo: <code>git push</code></li>
  <li>If you do not have direct write access to the <code>webwml</code> repository, 
  submit your changes with a <a href="#write-access-via-merge-request">merge request</a> or contact other developers for help.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Git Documentation</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Closing Debian Bugs in Git Commits</a></h4>

<p>
If you include <code>Closes: #</code><var>nnnnnn</var> in your commit log
entry, then bug number <code>#</code><var>nnnnnn</var> will be closed
automatically when you push your changes. The precise form of this is the same as
<a href="$(DOC)/debian-policy/ch-source.html#id24">in Debian policy</a>.</p>

<h4><a name="links-using-http-https">Linking using HTTP/HTTPS</a></h4>

<p>Many Debian websites support SSL/TLS, so please use HTTPS links where
possible and sensible. <strong>However</strong>, some
Debian/DebConf/SPI/etc websites either don't have have HTTPS support
or only use the SPI CA (and not an SSL CA trusted by all browsers). To
avoid causing error messages for non-Debian users, please do not link
to such sites using HTTPS.</p>

<p>The Git repository will reject commits containing plain HTTP links
for Debian websites that support HTTPS or containing HTTPS links for
the Debian/DebConf/SPI websites that are known to either not support
HTTPS or use certificates signed only by SPI.</p>

<h3><a name="translation-work">Working on Translations</a></h3>

<p>Translations should always be kept to be up-to-date with its
corresponding English file. The <code>translation-check</code> header in translation
files is used to track which version of English file the current
translation was based on. If you change translated files, you need to update the
<code>translation-check</code> header to match the Git commit hash of the
corresponding change in the English file. You can identify the hash with the
following command:</p>

<pre>
  git log path/to/english/file
</pre>

<p>If you do a new translation of a file, please use the <code>copypage.pl</code> script. 
It creates a template for your language, including the correct 
translation header.</p>

<h4><a name="translation-smart-change">Translation Changes with smart_change.pl</a></h4>

<p><code>smart_change.pl</code> is a script designed to make it easier
to update original files and their translations together. There are
two ways to use it, depending on what changes you are making.</p>

<p>
This is how to use <code>smart_change.pl</code> and how to just update the <code>translation-check</code> 
headers when you're working on files manually:
</p>

<ol>
  <li>Make the changes to the original file(s) and commit your changes.</li>
  <li>Update the translations.</li>
  <li>Run <code>smart_change.pl -c COMMIT_HASH</code> (use the commit hash of the changes in the original file(s)). 
  It will pick up the changes and update 
  headers in the translated files.</li>
  <li>Review the changes (e.g. with <code>git diff</code>).</li>
  <li>Commit the translation changes.</li>
</ol>

<p>
Alternatively, you can work with regular expression to make multiple changes across files in one pass:
</p>

<ol>
  <li>Run <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>Review the changes (e.g. with <code>git diff</code>).</li>
  <li>Commit the original file(s).</li>
  <li>Run <code>smart_change.pl origfile1 origfile2</code>
    (i.e. <strong>without the regexp</strong> this time). It will now
    just update the headers in the translated files.</li>
  <li>Finally, commit the translation changes.</li>
</ol>

<p>
Admittedly, this requires a bit more effort than the first example as it involves two commits, but 
unavoidable due to the way Git hashes work.
</p>

<h2><a id="write-access">Write Access to the Git Repository</a></h2>

<p>
The source code of the Debian website is managed with Git and located at 
<url https://salsa.debian.org/webmaster-team/webwml/>. 
By default, guests are not allowed to push commits to the source code repository. 
If you want to contribute to the Debian website, you need some kind of permission 
to gain write access to the repository.
</p>

<h3><a name="write-access-unlimited">Unlimited Write Access</a></h3>

<p>
If you need unlimited write access to the repository, e.g. if you're about to become 
a frequent contributor, please request write access via 
the <url https://salsa.debian.org/webmaster-team/webwml/> web interface after 
logging in to Debian's Salsa platform.
</p>

<p>
If you are new to Debian's website development and have no previous experience, 
please also send an email to <a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a> and introduce yourself before requesting 
unlimited write access. Would you be so kind and tell us more about yourself, for example, 
which part of the website you plan to work on, which languages you speak, 
and also if there is another Debian team member who can vouch for you.
</p>

<h3><a name="write-access-via-merge-request">Merge Requests</a></h3>

<p>
It's not necessary to get unlimited write access to the repository — 
you can always submit a merge request and let other 
developers review and accept your work. Please follow the standard procedure 
for merge requests provided by the Salsa GitLab platform via 
its web interface and read the following two documents:
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a></li>
</ul>

<p>
Please note that merge requests aren't monitored by all website developers. Therefore, it might 
take some time before you receive some feedback. If you wonder whether your contribution 
will get accepted or not, please send a email to the 
<a href="https://lists.debian.org/debian-www/">debian-www</a> 
mailing list and ask for a review.
</p>

<h2><a id="notifications">Getting Notifications</a></h2>

<p>
If you're working on the Debian website, you probably want know what's going on in the <code>webwml</code> repository. There are two ways to stay in the loop: commit notifications and merge request notifications.</p>

<h3><a name="commit-notifications">Receiving Commit Notifications</a></h3>

<p>We have configured the <code>webwml</code> project in Salsa so that commits are
shown in the IRC channel #debian-www.</p>

<p>
If you want to receive notifications about commits 
in the <code>webwml</code> repo via email, please subscribe to the <code>www.debian.org</code> 
pseudo package via tracker.debian.org and activate the <code>vcs</code> keyword 
there, following these steps (only once):</p>

<ol>
  <li>Open a web browser and go to <url https://tracker.debian.org/pkg/www.debian.org>.</li>
  <li>Subscribe to the <code>www.debian.org</code> pseudo package. (You can authenticate
      via SSO or register an email and password, if you aren't already using
      tracker.debian.org for other purposes).</li>
  <li>Go to <url https://tracker.debian.org/accounts/subscriptions/>, then to <code>modify
      keywords</code>, check <code>vcs</code> (if it's not checked) and save.</li>
  <li>From now on you will get emails when somebody commits to the
      <code>webwml</code> repo.</li>
</ol>

<h3><a name="merge-request-notifications">Receiving Merge Request Notifications</a></h3>

<p>
If you wish to receive notification emails whenever there are new merge
requests submitted on the <code>webwml</code> repository in Salsa, you may configure 
your notification settings on the web interface, following these steps:
</p>

<ol>
  <li>Log in to your Salsa account and go to the project page.</li>
  <li>Click the bell icon on the top of the project homepage.</li>
  <li>Select the notification level you prefer.</li>
</ol>
