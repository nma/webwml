<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

    <p>Zekun Shen and Brendan Dolan-Gavitt discovered a flaw in the
    mwifiex_usb_recv() function of the Marvell WiFi-Ex USB Driver. An
    attacker able to connect a crafted USB device can take advantage of
    this flaw to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

    <p>Sushma Venkatesh Reddy discovered a missing GPU TLB flush in the
    i915 driver, resulting in denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

    <p>Samuel Page and Eric Dumazet reported a stack overflow in the
    networking module for the Transparent Inter-Process Communication
    (TIPC) protocol, resulting in denial of service or potentially the
    execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0516">CVE-2022-0516</a>

    <p>It was discovered that an insufficient check in the KVM subsystem
    for s390x could allow unauthorized memory read or write access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0847">CVE-2022-0847</a>

    <p>Max Kellermann discovered a flaw in the handling of pipe buffer
    flags. An attacker can take advantage of this flaw for local
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22942">CVE-2022-22942</a>

    <p>It was discovered that wrong file descriptor handling in the
    VMware Virtual GPU driver (vmwgfx) could result in information leak
    or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

    <p>Lyu Tao reported a flaw in the NFS implementation in the Linux
    kernel when handling requests to open a directory on a regular file,
    which could result in a information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24959">CVE-2022-24959</a>

    <p>A memory leak was discovered in the yam_siocdevprivate() function of
    the YAM driver for AX.25, which could result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

    <p>Szymon Heidrich reported the USB Gadget subsystem lacks certain
    validation of interface OS descriptor requests, resulting in memory
    corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

    <p>Szymon Heidrich reported that the RNDIS USB gadget lacks validation
    of the size of the RNDIS_MSG_SET command, resulting in information
    leak from kernel memory.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.92-2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5092.data"
# $Id: $
