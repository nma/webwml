<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in yaws, a high performance HTTP 1.1
webserver written in Erlang.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24379">CVE-2020-24379</a>

    <p>The WebDAV implementation is prone to a XML External Entity (XXE)
    injection vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24916">CVE-2020-24916</a>

    <p>The CGI implementation does not properly sanitize CGI requests
    allowing a remote attacker to execute arbitrary shell commands via
    specially crafted CGI executable names.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.0.6+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your yaws packages.</p>

<p>For the detailed security status of yaws please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/yaws">https://security-tracker.debian.org/tracker/yaws</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4773.data"
# $Id: $
