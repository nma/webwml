<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an information disclosure issue in
python-treq, a high-level library/API for making HTTP requests using the
Twisted network programming library. HTTP cookies were not bound to a single
domain and were instead sent to every domain.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23607">CVE-2022-23607</a>

    <p>treq is an HTTP library inspired by requests but written on top of
    Twisted's Agents. Treq's request methods (`treq.get`, `treq.post`, etc.)
    and `treq.client.HTTPClient` constructor accept cookies as a dictionary.
    Such cookies are not bound to a single domain, and are therefore sent to
    *every* domain ("supercookies"). This can potentially cause sensitive
    information to leak upon an HTTP redirect to a different domain., e.g.
    should `https://example.com` redirect to `http://cloudstorageprovider.com`
    the latter will receive the cookie `session`. Treq 2021.1.0 and later bind
    cookies given to request methods (`treq.request`, `treq.get`,
    `HTTPClient.request`, `HTTPClient.get`, etc.) to the origin of the *url*
    parameter. Users are advised to upgrade. For users unable to upgrade
    Instead of passing a dictionary as the *cookies* argument, pass a
    `http.cookiejar.CookieJar` instance with properly domain- and scheme-scoped
    cookies in it.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
15.1.0-1+deb9u1.</p>

<p>We recommend that you upgrade your python-treq packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2954.data"
# $Id: $
