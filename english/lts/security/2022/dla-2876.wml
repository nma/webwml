<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues have been discovered in vim: an enhanced vi text editor:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17087">CVE-2017-17087</a>

    <p>fileio.c in Vim sets the group ownership of a .swp file to the editor's primary
    group (which may be different from the group ownership of the original file),
    which allows local users to obtain sensitive information by leveraging an
    applicable group membership.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20807">CVE-2019-20807</a>

    <p>Users can circumvent the rvim restricted mode and execute arbitrary OS
    commands via scripting interfaces (e.g., Python, Ruby, or Lua).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3778">CVE-2021-3778</a>

    <p>Heap-based Buffer Overflow with invalid utf-8 character was detected in
    regexp_nfa.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3796">CVE-2021-3796</a>

    <p>Heap Use-After-Free memory error was detected in normal.c. A successful
    exploitation may lead to code execution.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:8.0.0197-4+deb9u4.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>For the detailed security status of vim please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/vim">https://security-tracker.debian.org/tracker/vim</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2876.data"
# $Id: $
