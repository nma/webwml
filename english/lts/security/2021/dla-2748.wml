<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in tnef, a tool to unpack MIME application/ms-tnef
attachments.
Using emails with a crafted winmail.dat application/ms-tnef attachment
might allow an attacker to change .ssh/authorized_keys.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.4.12-1.2+deb9u1.</p>

<p>We recommend that you upgrade your tnef packages.</p>

<p>For the detailed security status of tnef please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tnef">https://security-tracker.debian.org/tracker/tnef</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2748.data"
# $Id: $
