<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial of service vulnerability in
the libjpeg-turbo CPU-optimised JPEG image library. A heap-based
buffer over-read could be triggered by a specially-crafted bitmap
(BMP) file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14498">CVE-2018-14498</a>

    <p>get_8bit_row in rdbmp.c in libjpeg-turbo through 1.5.90 and MozJPEG through 3.3.1 allows attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted 8-bit BMP in which one or more of the color indices is out of range for the number of palette entries.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.3.1-12+deb8u2.</p>

<p>We recommend that you upgrade your libjpeg-turbo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1719.data"
# $Id: $
