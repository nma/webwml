<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in the Tomcat servlet and JSP
engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1336">CVE-2018-1336</a>

  <p>An improper handing of overflow in the UTF-8 decoder with
  supplementary characters can lead to an infinite loop in the decoder
  causing a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8034">CVE-2018-8034</a>

  <p>The host name verification when using TLS with the WebSocket client
  was missing. It is now enabled by default.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8.0.14-1+deb8u13.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1491.data"
# $Id: $
