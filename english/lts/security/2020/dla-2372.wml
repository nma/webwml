<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial of service attack in libproxy, a
library to make applications HTTP proxy aware. A remote server could cause an
infinite stack recursion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25219">CVE-2020-25219</a>

    <p>url::recvline in url.cpp in libproxy 0.4.x through 0.4.15 allows a
    remote HTTP server to trigger uncontrolled recursion via a response
    composed of an infinite stream that lacks a newline character. This leads
    to stack exhaustion.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.4.14-2+deb9u1.</p>

<p>We recommend that you upgrade your libproxy packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2372.data"
# $Id: $
