<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Thomas Gerbet discovered that viewvc, a web interface for CVS and
Subversion repositories, did not properly sanitize user input. This
issue resulted in a potential Cross-Site Scripting vulnerability.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.5-1.4+deb7u1.</p>

<p>We recommend that you upgrade your viewvc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-820.data"
# $Id: $
