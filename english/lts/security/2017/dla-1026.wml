<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10971">CVE-2017-10971</a>

    <p>A user authenticated to an X Session could crash or execute code in the
    context of the X Server by exploiting a stack overflow in the endianness
    conversion of X Events.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10972">CVE-2017-10972</a>

    <p>Uninitialized data in endianness conversion in the XEvent handling of the
    X.Org X Server allowed authenticated malicious users to access potentially
    privileged data from the X server.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.12.4-6+deb7u7.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1026.data"
# $Id: $
