#use wml::debian::template title="Debian 11 -- Errata" BARETITLE=true
#use wml::debian::toc

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c5246a0b0d975c9fdec40315a561c3888d9c2088"

<toc-display/>


# <toc-add-entry name="known_probs">Bekannte Probleme</toc-add-entry>

<toc-add-entry name="security">Sicherheitsprobleme</toc-add-entry>

<p>Das Debian-Sicherheitsteam stellt Aktualisierungen von Paketen in der
stabilen Veröffentlichung bereit, in denen Sicherheitsprobleme erkannt wurden.
Bitte lesen Sie die <a href="$(HOME)/security/">Sicherheitsseiten</a>
bezüglich weiterer Informationen über alle Sicherheitsprobleme, die in
<q>Buster</q> erkannt wurden.</p>

<p>Wenn Sie APT verwenden, fügen Sie die folgende Zeile zu
<tt>/etc/apt/sources.list</tt> hinzu, um auf die neuesten
Sicherheitsaktualisierungen zugreifen zu können:

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>Führen Sie danach <kbd>apt update</kbd> gefolgt von
<kbd>apt upgrade</kbd> aus.</p>


<toc-add-entry name="pointrelease">Zwischenveröffentlichungen</toc-add-entry>

<p>Manchmal wird die veröffentlichte Distribution aktualisiert, wenn mehrere
   kritische Probleme aufgetreten oder Sicherheitsaktualisierungen
   herausgebracht worden sind. Im
   Allgemeinen wird dies als Zwischenveröffentlichung bezeichnet.</p>

<ul>
  <li>Die erste Zwischenveröffentlichung (11.1) wurde am
      <a href="$(HOME)/News/2021/20211009">09. Oktober 2021</a> veröffentlicht.</li>
</ul>

<ifeq <current_release_bullseye> 11.0 "

<p>Es gibt derzeit noch keine Zwischenveröffentlichungen für Debian 11.</p>" "

<p>Details über die Änderungen zwischen 11.0 und <current_release_bullseye/>
finden Sie im <a
href=http://http.us.debian.org/debian/dists/bullseye/ChangeLog>\
Änderungsprotokoll (Changelog)</a>.</p>"/>

<p>Korrekturen für die veröffentlichte stabile Distribution durchlaufen oft
eine ausgedehnte Testperiode, bevor sie im Archiv akzeptiert werden. Diese
Korrekturen sind allerdings im Verzeichnis
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> jedes Debian-Archiv-Spiegels verfügbar.</p>

<p>Falls Sie APT zur Aktualisierung Ihrer Pakete verwenden, können Sie diese
vorgeschlagenen Änderungen (proposed-updates) installieren, indem Sie folgende
Zeilen zu <tt>/etc/apt/sources.list</tt> hinzufügen:</p>

<pre>
  \# vorgeschlagene Änderungen für eine Debian 11 Zwischenveröffentlichung
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Führen Sie danach <kbd>apt update</kbd> gefolgt von
<kbd>apt upgrade</kbd> aus.</p>


<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
Informationen über Errata und Aktualisierungen für das Installationssystem
finden Sie auf der <a href="debian-installer/">Webseite zum Debian-Installer</a>.
</p>
