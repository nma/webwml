<define-tag pagetitle>Debian 11 aktualisiert: 11.2 veröffentlicht</define-tag>

<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="43ee4d624248d2eac95a5997ba86398be35914ec" maintainer="Erik Pfannenstein"
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>


<p>
Das Debian-Projekt freut sich, die zweite Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung bringt hauptsächlich Korrekturen für
Sicherheitsprobleme und Anpassungen für einige ernste Probleme. Für sie sind
bereits separate Sicherheitsankündigungen veröffentlicht worden; auf diese
wird, wo möglich, verwiesen.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version
von Debian <release> darstellt, sondern nur einige der enthaltenen Pakete
auffrischt. Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da
deren Pakete nach der Installation mit Hilfe eines aktuellen
Debian-Spiegelservers auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht
viele Pakete auf den neuesten Stand bringen müssen. Die meisten
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>
Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction authheaders "Neue fehlerkorrigierende Veröffentlichung der Originalautoren">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung 11.2 aktualisiert">
<correction bpftrace "Array-Indizierung überarbeitet">
<correction brltty "Operation unter X bei Verwendung von sysvinit überarbeitet">
<correction btrbk "Regression in der Aktualisierung für CVE-2021-38173 entfernt">
<correction calibre "Syntaxfehler korrigiert">
<correction chrony "Anbindung eines Sockets an ein Netzwerkgerät, welches einen Namen hat, der länger als drei Zeichen ist, für Call-Filter-Verwendung angepasst">
<correction cmake "PostgreSQL 13 zu den bekannten Versionen hinzugefügt">
<correction containerd "Neue stabile Veröffentlichung der Originalautoren; Umgang mit der Auswertung von uneindeutigen OCI-Manifesten verbessert [CVE-2021-41190]; <q>clone3</q> im Standard-seccomp-Profil unterstützen">
<correction curl "-ffile-prefix-map aus curl-config entfernt, um die Parallel-Installierbarkeit von libcurl4-gnutls-dev unter Multiarch zu verbessern">
<correction datatables.js "Unzureichende Maskierung von Arrays, die der Funktion zum Maskieren von HTML-Entitäten übergeben werden, nachgebessert [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware: TFTP-Serverpfad überarbeitet; Unterstützung für Einrichtung und Wartung von LTSP-chroot verbessert">
<correction debian-edu-doc "Debian-Bullseye-Handbuch aus dem Wiki aktualisiert; Übersetzungen aktualisiert">
<correction debian-installer "Neukompilierung gegen proposed-updates; Kernel-ABI auf -10 aktualisiert">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction distro-info-data "Enthaltene Daten für Ubuntu 14.04 und 16.04 ESM aktualisiert; Ubuntu 22.04 LTS hinzugefügt">
<correction docker.io "Mögliche Änderungen an Berechtigungen im Host-Dateisystem unterbunden [CVE-2021-41089]; strengere Dateiberechtigungen für /var/lib/docker vergeben [CVE-2021-41091]; Versenden von Zugangsdaten an die Standard-Registry unterbunden [CVE-2021-41092]; Unterstützung für <q>clone3</q>-Systemaufruf in Standard-seccomp-Policy hinzugefügt">
<correction edk2 "TOCTOU-Schwachstelle im Address Boot Guard [CVE-2019-11098]">
<correction freeipmi "pkgconfig-Dateien an die richtige Stelle kopieren">
<correction gdal "Unterstützung für BAG 2.0 Extract im LVBAG-Treiber überarbeitet">
<correction gerbv "Schreibzugriff außerhalb der Grenzen behoben [CVE-2021-40391]">
<correction gmp "Problem mit Ganzzahl- und Pufferüberlauf behoben [CVE-2021-43618]">
<correction golang-1.15 "Neue stabile Veröffentlichung der Originalautoren; <q>net/http: panic due to racy read of persistConn after handler panic</q> (net/http: Panik wegen Race Condition beim Auslesen der persistConn nach einer Handler-Panik) behoben [CVE-2021-36221]; <q>archive/zip: overflow in preallocation check can cause OOM panic</q> (archive/zp: Überlauf in der Allokations-Vorabprüfung kann OOM-Panik auslösen) behoben [CVE-2021-39293]; Pufferüberlauf behoben [CVE-2021-38297], Lesezugriff außerhalb der Grenzen behoben [CVE-2021-41771], Dienstblockade behoben [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Auswertung von GDAL-Formaten für den Fall korrigiert, dass die Beschreibung einen Strichpunkt enthält">
<correction horizon "Übersetzungen wiederhergestellt">
<correction htmldoc "Pufferüberlauf behoben [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "Fcitx5 Fcitx4 vorziehen">
<correction isync "Mehrere Pufferüberläufe behoben [CVE-2021-3657]">
<correction jqueryui "Probleme mit Ausführung von nicht vertrauenswürdigem Code behoben [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Absturz beim Verwenden des <q>Move</q>-(Verschieben)-Menüeintrags behoben">
<correction keepalived "Unnötig großzügige DBus-Richtlinie korrigiert [CVE-2021-44225]">
<correction keystone "Informationsleck, das es ermöglichte, die Existenz von Benutzern festzustellen, geflickt [CVE-2021-38155]; einige Verbesserungen an der Geschwindigkeit in der keystone-uwsgi.ini">
<correction kodi "Pufferüberläufe in PLS-Wiedergabelisten behoben [CVE-2021-42917]">
<correction libayatana-indicator "Icons beim Laden aus einer Datei skalieren; regelmäßige Abstürze in Indikator-Applets unterbunden">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libencode-perl "Speicherleck in Encode.xs geflickt">
<correction libseccomp "Unterstützung für Systemaufrufe bis Linux 5.15 hinzugefügt">
<correction linux "Neue Veröffentlichung der Originalautoren; ABI auf 10 angehoben; RT: Aktualisierung auf 5.10.83-rt58">
<correction linux-signed-amd64 "Neue Veröffentlichung der Originalautoren; ABI auf 10 angehoben; RT: Aktualisierung auf 5.10.83-rt58">
<correction linux-signed-arm64 "Neue Veröffentlichung der Originalautoren; ABI auf 10 angehoben; RT: Aktualisierung auf 5.10.83-rt58">
<correction linux-signed-i386 "Neue Veröffentlichung der Originalautoren; ABI auf 10 angehoben; RT: Aktualisierung auf 5.10.83-rt58">
<correction lldpd "Problem mit Heap-Überlauf behoben [CVE-2021-43612]; kein VLAN-Tag setzen, wenn der Client es nicht gesetzt hat">
<correction mrtg "Fehler in Variablennamen korrigiert">
<correction node-getobject "Prototype Pollution behoben [CVE-2020-28282]">
<correction node-json-schema "Prototype Pollution behoben [CVE-2021-3918]">
<correction open3d "Sichergestellt, dass python3-open3d von python3-numpy abhängt">
<correction opendmarc "opendmarc-import überarbeitet; unterstützte Maximallänge der Tokens in ARC_Seal-Kopfzeilen erhöht, um Abstürze abzustellen">
<correction plib "Ganzzahlüberläufe behoben [CVE-2021-38714]">
<correction plocate "Problem behoben, durch das Nicht-ASCII-Zeichen falsch maskiert würden">
<correction poco "Installation der CMake-Dateien korrigiert">
<correction privoxy "Speicherlecks behoben [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542]; Anfälligkeit für sitenübergreifendes Scripting behoben [CVE-2021-44543]">
<correction publicsuffix "Enthaltene Daten aktualisiert">
<correction python-django "Neue Sicherheitsveröffentlichung der Originalautoren: potenzielle Umgehungsmöglichkeit einer Upstream-Zugriffskontrolle, die auf URL-Pfaden basiert [CVE-2021-44420]">
<correction python-eventlet "Kompatibilität mit dnspython 2 verbessert">
<correction python-virtualenv "Absturz bei Verwendung von --no-setuptools behoben">
<correction ros-ros-comm "Dienstblockade behoben [CVE-2021-37146]">
<correction ruby-httpclient "Zertifikatsspeicher des Systems benutzen">
<correction rustc-mozilla "Neues Quellpaket, welches die Kompilierung neuerer firefox-esr- und thunderbird-Versionen unterstützt">
<correction supysonic "jquery symbolisch verlinkt statt es direkt zu laden; symbolische Verknüpfung der minimierten bootstrap-CSS-Dateien korrigiert">
<correction tzdata "Daten für Fiji und Palästina aktualisiert">
<correction udisks2 "Einhängeoptionen: Immer errors=remount-ro für ext-Dateisysteme verwenden [CVE-2021-3802]; mkfs-Befehl zum Formatieren von exfat-Partitionen verwenden; Empfehlung für exfatprogs als vorrangige Alternative hinzugefügt">
<correction ulfius "Verwendung eigener Allokatoren mit ulfius_url_decode und ulfius_url_encode überarbeitet">
<correction vim "Heap-Überläufe [CVE-2021-3770 CVE-2021-3778] und Verwendung von freigegebenem Speicher [CVE-2021-3796] behoben; vim-gtk-Alternativen für die Umstellung vim-gtk -&gt; vim-gtk3 entfernt, um Upgrades von Buster zu erleichtern">
<correction wget "Downloads größer als 2GB auf 32-Bit-Systemen überarbeitet">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>
Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:
</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>
Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Die derzeitige Stable-Distribution:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Vorgeschlagene Aktualisierungen für die Stable-Distribution:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Sicherheitsankündigungen und -informationen:
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt; oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>


