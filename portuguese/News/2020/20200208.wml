#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920"
<define-tag pagetitle>Debian 10 atualizado: 10.3 lançado</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a terceira atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, junto com alguns ajustes para problemas sérios. Avisos de segurança
já foram publicados em separado e são referenciados quando disponíveis.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de se desfazer das antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>As pessoas que frequentemente instalam atualizações de security.debian.org
não terão que atualizar muitos pacotes, e a maioria dessas atualizações estão
incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
ao apontar o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction alot "Remove prazo de expiração de chaves da suíte de testes, corrigindo falha de construção">
<correction atril "Corrige falha de segmentação quando nenhum documento é carregado; corrige a leitura de memória não inicializada [CVE-2019-11459]">
<correction base-files "Atualizações para versão pontual">
<correction beagle "Fornecer um roteiro de empacotamento em vez de links simbólicos para os JARs, fazendo-os funcionar novamente">
<correction bgpdump "Corrige falha de segmentação">
<correction boost1.67 "Corrige comportamento indefinido que leva à quebra de libboost-numpy">
<correction brightd "Compara de fato o valor lido de /sys/class/power_supply/AC/online com <q>0</q>">
<correction casacore-data-jplde "Inclui tabelas até 2040">
<correction clamav "Nova versão upstream; corrige falha de negação de serviço [CVE-2019-15961]; remove opção ScanOnAccess substituindo por clamonacc">
<correction compactheader "Nova versão upstream compatível com Thunderbird 68">
<correction console-common "Corrige regressão que faz arquivos não serem inclusos">
<correction csh "Corrige falha de segmentação em eval">
<correction cups "Corrige vazamento de memória em ppdOpen; corrige validação de linguagem padrão em ippSetValuetag [CVE-2019-2228]">
<correction cyrus-imapd "Adiciona tipo BACKUP em cyrus-upgrade-db, corrigindo questões de atualização">
<correction debian-edu-config "Mantém configurações de proxy no cliente se WPAD estiver inalcançável">
<correction debian-installer "Reconstruído de acordo com proposed-updates (atualizações propostas); ajusta a geração de mini.iso no arm para que netboot EFI funcione; atualização do padrão USE_UDEBS_FROM de instável (unstable) para buster, para ajudar usuários(as) a realizarem construções locais">
<correction debian-installer-netboot-images "Reconstruído de acordo com proposed-updates (atualizações propostas)">
<correction debian-security-support "Atualizado status de suporte de segurança de diversos pacotes">
<correction debos "Reconstruído de acordo com golang-github-go-debos-fakemachine">
<correction dispmua "Nova versão upstream compatível com Thunderbird 68">
<correction dkimpy "Nova versão estável upstream">
<correction dkimpy-milter "Corrige gerenciamento de privilégios na inicialização para que sockets Unix funcionem">
<correction dpdk "Nova versão upstream estável">
<correction e2fsprogs "Corrige problema de potencial esvaziamento de pilha no e2fsck [CVE-2019-5188]; corrige uso depois de livre em e2fsck">
<correction fig2dev "Permitir que sequências de texto Fig v2 terminem com múltiplos ^A [CVE-2019-19555]; rejeitar setas de tipos grandes causando estouro de inteiros [CVE-2019-19746]; corrige diversos erros [CVE-2019-19797]">
<correction freerdp2 "Corrige manipulação de retorno de realocação [CVE-2019-17177]">
<correction freetds "tds: Assegura que UDT tenha varint configurado para 8 [CVE-2019-13508]">
<correction git-lfs "Corrige problemas de compilação com versões recentes de Go">
<correction gnubg "Incrementa o tamanho de buffers estáticos utilizados para construção de mensagens durante início do programa de forma que a tradução para espanhol não estoure um buffer">
<correction gnutls28 "Corrige problemas de interoperação com gnutls 2.x; corrige análise de certificados usando RegisteredID">
<correction gtk2-engines-murrine "Corrige coinstalação com outros temas">
<correction guile-2.2 "Corrige falhas de compilação">
<correction libburn "Corrige <q>gravação multi-trilha cdrskin que estava lenta e foi suspensa após a trilha 1</q>">
<correction libcgns "Corrige falha de compilação em ppc64el">
<correction libimobiledevice "Trata corretamente as escritas SSL parciais">
<correction libmatroska "Aumenta a dependência de bibliotecas compartilhadas para 1.4.7 já que esta versão introduz novos símbolos">
<correction libmysofa "Correções de segurança [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl "Corrige interpretação de anos de 2020 em diante">
<correction libparse-win32registry-perl "Corrige interpretação de anos de 2020 em diante">
<correction libperl4-corelibs-perl "Corrige interpretação de anos de 2020 em diante">
<correction libsolv "Corrige estouro de buffer de pilha [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "Corrige planilha OpenDocument previamente inutilizada e passagem de opções de formatação JSON">
<correction libtimedate-perl "Corrige interpretação de anos de 2020 em diante">
<correction libvirt "Apparmor: permite execução do pygrub; não renderiza osxsave, ospke na linha de comando do QEMU; isso ajuda versões mais recentes do QEMU com algumas configurações geradas pelo virt-install">
<correction libvncserver "RFBserver: não vaza pilha de memória para o lado remoto [CVE-2019-15681]; resolve travamentos durante encerramento de conexão e uma falha de segmentação em servidores VNC multi-tarefa; corrige problemas de conexão a servidores VMWare; corrige falha de x11vnc quando vncviewer se conecta">
<correction limnoria "Corrige divulgação de informação remota e possibilidade de execução remota de código na extensão Math [CVE-2019-19010]">
<correction linux "Nova versão upstream estável">
<correction linux-latest "Atualização para ABI kernel Linux 4.19.0-8">
<correction linux-signed-amd64 "Nova versão upstream estável">
<correction linux-signed-arm64 "Nova versão upstream estável">
<correction linux-signed-i386 "Nova versão upstream estável">
<correction mariadb-10.3 "Nova versão upstream estável [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "Invoca shmget() com permissão 0600 em vez de 0777 [CVE-2019-5068]">
<correction mnemosyne "Adiciona dependência ausente em PIL">
<correction modsecurity "Corrige bug de análise de cabeçalho de cookie [CVE-2019-19886]">
<correction node-handlebars "Revoga chamada direta para <q>helperMissing</q> e <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-kind-of "Corrige verificação de vulnerabilidade de tipo em ctorName() [CVE-2019-20149]">
<correction ntpsec "Corrige tentativas lentas de DNS; corrige ntpdate -s (syslog) para ajuste do engate de if-up; correções de documentação">
<correction numix-gtk-theme "Corrige coinstalabilidade com outros temas">
<correction nvidia-graphics-drivers-legacy-340xx "Nova versão upstream estável">
<correction nyancat "Reconstruído em ambiente limpo para adicionar unidade systemd para nyancat-server">
<correction openjpeg2 "Corrige estouro de pilha [CVE-2018-21010] e estouro de inteiro [CVE-2018-20847]">
<correction opensmtpd "Avisa usuários(as) de mudanças na sintaxe do smtpd.conf (em versões anteriores); instala smtpctl setgid opensmtpq; trata códigos de saída não zero de nome de host durante a fase de configuração">
<correction openssh "Nega (de forma não fatal) ipc no seccomp sandbox, corrigindo falhas com OpenSSL 1.1.1d e kernel Linux &lt; 3.19 em algumas arquiteturas">
<correction php-horde "Corrige problema de cross-site scripting armazenado em Horde Cloud Block [CVE-2019-12095]">
<correction php-horde-text-filter "Corrige expressão regular inválida">
<correction postfix "Nova versão upstream estável">
<correction postgresql-11 "Nova versão upstream estável ">
<correction print-manager "Corrige falha se CUPS retornar a mesma identificação para múltiplos trabalhos de impressão">
<correction proftpd-dfsg "Corrige problema com [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "Corrige caminho para fontes">
<correction python-evtx "Corrige importação de <q>hexdump</q>">
<correction python-internetarchive "Fecha o arquivo depois de obter hash, evitando exaustão do descritor de arquivo">
<correction python3.7 "Correções de segurança [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "Adiciona suporte a impressão não PPD e evita retorno silencioso para um impressora com suporte PPD; corrige falha quando utilizado QLabels com rich text; corrige eventos sobre itens em tablets">
<correction qtwebengine-opensource-src "Corrige processamento de PDF; desabilita pilha de execução">
<correction quassel "Corrige negações quasselcore AppArmor quando a configuração é salva; corrige canal padrão para o Debian; remove arquivos de notícias (NEWS) desnecessários">
<correction qwinff "Corrige falha devido à detecção incorreta de arquivo">
<correction raspi3-firmware "Corrige detecção de console serial com kernel 5.x">
<correction ros-ros-comm "Corrige questões de segurança [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "Nova versão upstream estável; corrige permissões inseguras na extensão enigma [CVE-2018-1000071]">
<correction schleuder "Corrige reconhecimento de palavras-chave em mensagens com <q>cabeçalhos protegidos</q> e assunto vazio; remove as assinaturas não autoassinadas ao atualizar ou buscar chaves; erro se o argumento fornecido para `refresh_keys` não for uma lista existente; adiciona cabeçalho de lista de identificação ausente nos e-mails de notificação enviados aos(às) administradores(as); trata os problemas de decodificação cuidadosamente; padrão para codificação ASCII-8BIT">
<correction simplesamlphp "Corrige incompatibilidade PHP 7.3">
<correction sogo-connector "Nova versão upstream compatível com Thunderbird 68">
<correction spf-engine "Corrige gerenciamento de privilégios na inicialização de forma que os sockets Unix funcionem; atualiza documentação para TestOnly">
<correction sudo "Corrige um estouro de buffer (não explorável no buster) quando pwfeedback está habilitado e a entrada não é um terminal [CVE-2019-18634]">
<correction systemd "Configura fs.file-max sysctl para LONG_MAX em vez de ULONG_MAX; transfere a propriedade/modo do diretório de execução também para usuários(as) estáticos(as), assegurando que a execução de diretórios como CacheDirectory e StateDirectory esteja apropriadamente configurada para o(a) usuário(a) especificado(a) em User= antes de lançar o serviço">
<correction tifffile "Corrige script envoltório (wrapper)">
<correction tigervnc "Correções de segurança [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Correções de segurança [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "Corrige caminhos para ip(6)tables-restore em vista da migração para nftables">
<correction unhide "Corrige exaustão de pilha">
<correction x2goclient "Remove ~/, ~user{,/}, ${HOME}{,/} and $HOME{,/} do caminho de destino em modo SCP; corrige regressão com novas versões de libssh com correções para CVE-2019-14889 aplicadas">
<correction xmltooling "Corrige condição de corrida que poderia levar a falha sob carga">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de Segurança já lançou um aviso para cada uma dessas
atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>

<h2>Pacotes removidos </h2>

<p>Os seguintes pacotes foram removidos devido a circunstâncias além de nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction caml-crush "[armel] Impossível compilar devido à falta de ocaml-native-compilers">
<correction firetray "Incompatível com versões atuais do Thunderbird">
<correction koji "Questões de segurança">
<correction python-lamson "Quebrado por mudanças no python-daemon">
<correction radare2 "Questões de segurança; upstream não fornece suporte estável">
<correction radare2-cutter "Depende de radare2, que está para ser removido">
</table>

<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>informações da versão estável (stable) (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
