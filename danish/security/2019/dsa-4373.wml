#use wml::debian::translation-check translation="08e97d6a66338b9fb8da51eb27b4c3dde971c164" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder er opdaget i coTURN, en TURN- og STUN-server til 
VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

    <p>En SQL-indsprøjtningssårbarhed blev opdaget i coTURN's 
    administratorwebportal.  Da administrationswebgrænsefladen deles med 
    produktion, er det desværre ikke muligt at på en let måde at bortfiltrere 
    adgang udefra, og denne sikkerhedsopdatering deaktiverer fuldstændig 
    webgrænsefladen.  Brugerne bør i stedet benytte den lokale 
    kommandolinjegrænseflade.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

    <p>Standardopsætningen aktiverer usikker loopback-viderestilling.  En 
    fjernangriber med adgang til TURN-grænsefladen kunne udnytte sårbarheden til 
    at opnå adgang til tjenester, der kun bør være lokale.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

    <p>Standardopsætningen anvender en tom adgangskode til den lokale 
    kommandolinjebaserede administrationsgrænseflade.  En angriber med adgang 
    til den lokale konsol (enten en lokal angriber eller en fjern angriber, der 
    drager nytte af 
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
    kunne forøge rettigheder til administrator af coTURN-serveren.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 4.5.0.5-1+deb9u1.</p>

<p>Vi anbefaler at du opgraderer dine coturn-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende coturn, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4373.data"
