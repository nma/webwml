msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:52+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "i pakke"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "markeret"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "med alvorlighedsgrad"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "i kildekodepakke"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "i pakker vedligeholdt af"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "indsendt af"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "ejes af"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "med status"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "med mail fra"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "nyeste fejl"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "med emne indeholdende"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "med afventerstatus"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "med indsender indeholdende"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "med videresendt indeholdende"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "med ejer indeholdende"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "med pakke"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "gammel visning"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "rå"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "alder"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Gentag sammenlagt"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Vend fejl om"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Omvendt afventer"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Omvendt alvorlighed"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Ingen fejl som påvirker pakker"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Ingen"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testing"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Uarkiveret"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Arkiveret"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Arkiveret og uarkiveret"

#~ msgid "Flags:"
#~ msgstr "Flag:"

#~ msgid "active bugs"
#~ msgstr "aktive fejl"

#~ msgid "display merged bugs only once"
#~ msgstr "vis kun flettede fejl én gang"

#~ msgid "no ordering by status or severity"
#~ msgstr "ingen sortering på status/alvorlighedsgrad"

#~ msgid "don't show table of contents in the header"
#~ msgstr "vis ikke indholdsfortegnelse øverst"

#~ msgid "don't show statistics in the footer"
#~ msgstr "vis ikke statistik i bunden"

#~ msgid "proposed-updates"
#~ msgstr "proposed-updates"

#~ msgid "testing-proposed-updates"
#~ msgstr "testing-proposed-updates"

#~ msgid "Package version:"
#~ msgstr "Pakkeversion:"

#~ msgid "Distribution:"
#~ msgstr "Distribution:"

#~ msgid "bugs"
#~ msgstr "fejl"

#~ msgid "open"
#~ msgstr "åben"

#~ msgid "forwarded"
#~ msgstr "videresendt"

#~ msgid "pending"
#~ msgstr "i gang"

#~ msgid "fixed"
#~ msgstr "rettet"

#~ msgid "done"
#~ msgstr "færdig"

#~ msgid "Include status:"
#~ msgstr "Status indeholder: "

#~ msgid "Exclude status:"
#~ msgstr "Status indeholder ikke: "

#~ msgid "critical"
#~ msgstr "kritisk"

#~ msgid "grave"
#~ msgstr "graverende"

#~ msgid "serious"
#~ msgstr "alvorlig"

#~ msgid "important"
#~ msgstr "vigtig"

#~ msgid "minor"
#~ msgstr "mindre"

#~ msgid "wishlist"
#~ msgstr "ønskeliste"

#~ msgid "Include severity:"
#~ msgstr "Alvorlighedsgrad indeholder: "

#~ msgid "Exclude severity:"
#~ msgstr "Alvorlighedsgrad indeholder ikke: "

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignorer"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignorer"

#~ msgid "lenny"
#~ msgstr "lenny"

#~ msgid "lenny-ignore"
#~ msgstr "lenny-ignorer"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "confirmed"
#~ msgstr "bekræftet"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "fixed-in-experimental"
#~ msgstr "rettet-i-eksperimentel"

#~ msgid "fixed-upstream"
#~ msgstr "rettet-opstrøm"

#~ msgid "help"
#~ msgstr "hjælp"

#~ msgid "l10n"
#~ msgstr "l10n"

#~ msgid "moreinfo"
#~ msgstr "mere info"

#~ msgid "patch"
#~ msgstr "patch"

#~ msgid "security"
#~ msgstr "sikkerhed"

#~ msgid "unreproducible"
#~ msgstr "ikke reproducérbar"

#~ msgid "upstream"
#~ msgstr "opstrøm"

#~ msgid "wontfix"
#~ msgstr "vil ikke rette"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "Include tag:"
#~ msgstr "Mærke indeholder: "

#~ msgid "Exclude tag:"
#~ msgstr "Mærke indeholder ikke: "
