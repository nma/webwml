#use wml::debian::template title="Debian en el escritorio" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5946007f07ed802a4ea96fe8dfa5ef2681f41b18"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Nuestra filosofía</a></li>
<li><a href="#help">Cómo puede ayudar</a></li>
<li><a href="#join">Únase a nosotros</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop es un grupo de voluntarios que desean crear el mejor sistema operativo posible para uso tanto en el hogar como en el entorno corporativo. Nuestro lema: <q>Software que funciona</q>. Nuestra meta: lograr la adopción generalizada de Debian, GNU y Linux.</p>
</aside>

<h2><a id="philosophy">Nuestra filosofía</a></h2>

<p>
Reconocemos que existen
muchos <a href="https://wiki.debian.org/DesktopEnvironment">
entornos de escritorio</a> y queremos dar soporte a su uso, lo que incluye
asegurarse de que funcionen bien en Debian. Nuestro objetivo es hacer las interfaces
gráficas fáciles de usar para principiantes, permitiendo al mismo tiempo que quienes tengan conocimientos
avanzados modifiquen cosas si lo desean.
</p>

<p>
Intentaremos asegurar que el software esté configurado para los usos más comunes
de un sistema de escritorio. Por ejemplo, la cuenta de usuario creada durante la
instalación debería tener los permisos suficientes para reproducir sonido e imágenes, para imprimir
y para administrar el sistema a través de sudo. También nos gustaría reducir
al mínimo el número de preguntas de <a href="https://wiki.debian.org/debconf">debconf</a>
(el sistema de gestión de la configuración de Debian).
No hay necesidad de presentar complicados detalles técnicos durante la
instalación. Por el contrario, intentaremos asegurarnos de que las preguntas de debconf
tengan sentido para los usuarios y las usuarias. Un usuario novel puede no entender siquiera a qué
se refieren esas preguntas. Un experto, sin embargo, no tendrá inconveniente en configurar
el entorno de escritorio después de completar la instalación.
</p>

<h2><a id="help">Cómo puede ayudar</a></h2>

<p>
Buscamos personas motivadas, que hagan realidad las cosas. No tiene
que ser un desarrollador o desarrolladora de Debian (DD, por sus siglas en inglés) para enviar parches o hacer paquetes. El
núcleo del equipo «Debian Desktop» se asegurará de que su trabajo se integre en el sistema.
Estas son algunas de las cosas que puede hacer para colaborar:
</p>

<ul>
  <li>Probar el entorno de escritorio por omisión (o cualquier otro entorno de escritorio) de la próxima versión. Descargue una de las <a href="$(DEVEL)/debian-installer/">imágenes «en pruebas»</a> y envíe comentarios a la <a href="https://lists.debian.org/debian-desktop/">lista de correo debian-desktop</a>.</li>
  <li>Unirse al <a href="https://wiki.debian.org/DebianInstaller/Team">equipo del instalador de Debian</a> y ayudar a mejorar el <a href="$(DEVEL)/debian-installer/">instalador</a> – la interfaz GTK+ le necesita.</li>
  <li>Ayudar a los equipos <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME</a>, <a href="https://qt-kde-team.pages.debian.net/">mantenedores de Debian Qt/KDE y Debian KDE Extras</a> o <a href="https://salsa.debian.org/xfce-team/">grupo Debian Xfce</a> en el empaquetado, corrección de fallos, documentación, pruebas u otras tareas.</li>
  <li>Ayudar a mejorar <a href="https://packages.debian.org/debconf">debconf</a> rebajando la prioridad de preguntas o eliminando preguntas innecesarias de los paquetes. Hacer que las preguntas necesarias resulten más fáciles de entender.</li>
  <li>Trabajar en las <a href="https://wiki.debian.org/DebianDesktop/Artwork">obras artísticas para el escritorio de Debian</a> si dispone de habilidades como diseñador o diseñadora.</li>
</ul>

<h2><a id="join">Únase a nosotros</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>Wiki:</strong> visite nuestra wiki <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> (algunos artículos pueden estar desactualizados).</li>
  <li><strong>Lista de correo:</strong> participe en los debates de la lista de correo <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</li>
  <li><strong>Canal IRC:</strong> charle con nosotros en IRC. Únase al canal #debian-desktop en <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org)</li>
</ul>

