#use wml::debian::translation-check translation="1fe97ede528cadc78447b1edeb9a753b00b0639c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30506">CVE-2021-30506</a>

    <p>@retsew0x01 descubrió un error en la interfaz de instalación de apps web.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30507">CVE-2021-30507</a>

    <p>Alison Huffman descubrió un error en el modo Offline.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30508">CVE-2021-30508</a>

    <p>Leecraso y Guang Gong descubrieron un problema de desbordamiento de memoria en la implementación
    de Media Feeds.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30509">CVE-2021-30509</a>

    <p>David Erceg descubrió un problema de escritura fuera de límites en la implementación
    de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30510">CVE-2021-30510</a>

    <p>Weipeng Jiang descubrió una condición de carrera en el gestor de ventanas aura.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30511">CVE-2021-30511</a>

    <p>David Erceg descubrió un problema de lectura fuera de límites en la implementación
    de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30512">CVE-2021-30512</a>

    <p>ZhanJia Song descubrió un problema de «uso tras liberar» en la implementación de
    las notificaciones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30513">CVE-2021-30513</a>

    <p>Man Yue Mo descubrió un tipo incorrecto en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30514">CVE-2021-30514</a>

    <p>koocola y Wang descubrieron un problema de «uso tras liberar» en la funcionalidad de llenado automático
    («Autofill»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30515">CVE-2021-30515</a>

    <p>Rong Jian y Guang Gong descubrieron un problema de «uso tras liberar» en la API de
    acceso al sistema de archivos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30516">CVE-2021-30516</a>

    <p>ZhanJia Song descubrió un problema de desbordamiento de memoria en el historial de navegación.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30517">CVE-2021-30517</a>

    <p>Jun Kokatsu descubrió un problema de desbordamiento de memoria en el modo «reader».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30518">CVE-2021-30518</a>

    <p>laural descubrió el uso de un tipo incorrecto en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30519">CVE-2021-30519</a>

    <p>asnine descubrió un problema de «uso tras liberar» en la funcionalidad Payments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30520">CVE-2021-30520</a>

    <p>Khalil Zhani descubrió un problema de «uso tras liberar» en la implementación de
    Tab Strip.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 90.0.4430.212-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4917.data"
