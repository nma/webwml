#use wml::debian::translation-check translation="044f0b378d1e97d512ed4133d9cfa3de84daad8b" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности LTS</define-tag>
<define-tag moreinfo>

<p>В mutt, консольном клиенте электронной почты, были обнаружены две уязвимости.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14093">CVE-2020-14093</a>

    <p>Mutt позволяет выполнять атаку по принципу человек-в-середине на IMAP-команды
    fcc/postpone через PREAUTH-ответ.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14954">CVE-2020-14954</a>

    <p>Mutt содержит проблему буферизации STARTTLS, которая касается IMAP, SMTP и
    POP3. Когда сервер отправляет ответ <q>begin TLS</q>, клиент считывает
    дополнительные данные (например, от злоумышленника в середине) и
    выполняет их в TLS-контексте, что также известно как <q>введение ответа</q>.</p>

</ul>

<p>В Debian 8 <q>Jessie</q> эти проблемы были исправлены в версии
1.5.23-3+deb8u2.</p>

<p>Рекомендуется обновить пакеты mutt.</p>

<p>Дополнительную информацию о рекомендациях по безопасности Debian LTS,
о том, как применять эти обновления к вашей системе, а также ответы на часто
задаваемые вопросы можно найти по адресу: <a href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2268.data"
