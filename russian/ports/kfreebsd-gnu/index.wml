#use wml::debian::template title="Debian GNU/kFreeBSD"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f" maintainer="Lev Lamberov"

#use wml::debian::toc

<toc-display/>

<p>Перенос Debian GNU/kFreeBSD состоит из <a
href="https://www.gnu.org/">пользовательского окружения GNU</a>, использующего <a
href="https://www.gnu.org/software/libc/">библиотеку C GNU</a>, которое запущено
поверх ядра
<a href="https://www.freebsd.org/">FreeBSD</a>, а также привычного <a
href="https://packages.debian.org/">набора пакетов Debian</a>.</p>

<div class="important">
<p>Debian GNU/kFreeBSD не является официально поддерживаемой
архитектурой. Этот перенос был выпущен в составе Debian 6.0 (Squeeze) и 7.0
(Wheezy) как <em>технологическая проба</em> и первый отличный от Linux
перенос. С Debian 8 (Jessie) он более не входит в число официальных
выпусков.</p>
</div>

<toc-add-entry name="resources">Ресурсы</toc-add-entry>

<p>Подробная информация о переносе (включая FAQ) находится на
wiki-странице
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>.
</p>

<h3>Списки рассылки</h3>
<p><a href="https://lists.debian.org/debian-bsd">Список рассылки Debian GNU/k*BSD</a>.</p>
<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">IRC-канал #debian-kbsd</a> (на irc.debian.org).</p>

<toc-add-entry name="Development">Разработка</toc-add-entry>

<p>Так как мы используем Glibc, проблемы переносимости тривиальны и, в большинстве случаев,
решаются простым копированием тестового набора для k*bsd*-gnu из другой,
основанной на Glibc, системы (такой как GNU или GNU/Linux). Дополнительная информация
приведена в документе
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">переноса</a>.</p>

<p>Также посмотрите список
<a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">текущих задач</a>,
чтобы узнать детали о том, что необходимо сделать.</p>

<toc-add-entry name="availablehw">Оборудование, доступное разработчикам Debian</toc-add-entry>

<p>Для работы над переносом разработчикам Debian предоставляется машина
lemon.debian.net (kfreebsd-amd64)
Подробную информацию об этих машинах смотрите в
<a href="https://db.debian.org/machines.cgi">базе данных машин Debian</a>.
В основном, вы можете использовать
два chroot-окружениями: testing и unstable. Обратите внимание на то, что эти системы пока
не администрируются членами команды DSA, поэтому <b>не отправляйте запросы для
debian-admin по этому поводу</b>. Вместо этого используйте <email "admin@lemon.debian.net">.</p>
