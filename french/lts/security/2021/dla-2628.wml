#use wml::debian::translation-check translation="1cca1c9ad2790c6514760d036e4a25e4b59b540f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans python2.7.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16935">CVE-2019-16935</a>

<p>Le serveur de documentation XML-RPC dans Python 2.7 avait une vulnérabilité
de script intersite à l’aide du champ server_title. Cela était possible dans
Lib/DocXMLRPCServer.py dans Python 2.x et dans Lib/xmlrpc/server.py dans
Python 3.x. Si set_server_title était appelée avec une entrée non autorisée, du
JavaScript arbitraire pouvait être fourni aux clients qui visitent l’URL http de
ce serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>

<p>Python2.7 était vulnérable à un empoisonnement de cache web à l’aide
de urllib.parse.parse_qsl et urllib.parse.parse_qs en utilisant une
dissimulation des paramètres de vecteur demandés. Lorsque l’attaquant pouvait
séparer des paramètres de requête en utilisant des points-virgules (;), il
pouvait provoquer une différence dans l’interprétation de la requête entre le
mandataire (en cours avec la configuration par défaut) et le serveur. Cela
pouvait aboutir à des requêtes malveillantes entièrement mises en cache de la
même façon que celles sûres, car le mandataire habituellement ne voyait pas
le point-virgule comme un séparateur et par conséquent ne l’incluait pas dans
la clé de cache d’un paramètre sans clé.</p>

<p>**Attention, modification d’API !**
Veuillez vérifier que vos logiciels fonctionnent correctement s’ils utilisent
<q>urllib.parse.parse_qs</q> ou <q>urllib.parse.parse_qsl</q>, <q>cgi.parse</q>
ou <q>cgi.parse_multipart</q>.</p>

<p>Les précédentes versions de Python permettaient d’utiliser à la
fois <q>;</q> et <q>&amp;</q> comme séparateur de paramètres de requête dans
<q>urllib.parse.parse_qs</q> et <q>urllib.parse.parse_qsl</q>. Pour des raisons
de sécurité et pour se conformer aux nouvelles recommandations W3C, cela a été
modifié pour utiliser une unique clé de séparateur avec <q>&amp;</q> par défaut.
Cette modification affecte aussi <q>cgi.parse</q> et <q>cgi.parse_multipart</q>
car elles utilisent en interne les fonctions affectées. Pour davantage de
détails, veuillez consulter leur documentation respective.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.7.13-2+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python2.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python2.7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python2.7">\
https://security-tracker.debian.org/tracker/python2.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2628.data"
# $Id: $
