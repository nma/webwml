#use wml::debian::translation-check translation="1381257b36af9714bcff3fc1a6dfc01d8318219a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans gerbv, un visualisateur de
fichiers Gerber au format RS-274X.</p>

<p>Une vulnérabilité d'écriture hors limites existe dans l'outil T-code du
format drill. Un fichier drill contrefait pour l'occasion peut conduire à
l'exécution de code. Un attaquant peut exploiter cette vulnérabilité en
fournissant un fichier malveillant.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.6.1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gerbv.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gerbv, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gerbv">\
https://security-tracker.debian.org/tracker/gerbv</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2839.data"
# $Id: $
