#use wml::debian::translation-check translation="8461642cd206a7ebfe69f01254b714b608eab3bd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service dans
<tt>ruby-websocket-extensions</tt>, une bibliothèque pour gérer des connexions
<q>Websocket</q> HTTP de longue durée.</p>

<p>L’analyseur prenait un temps quadratique lors de l’analyse d’un en-tête
contenant une valeur de chaîne de paramètre non <q>fermée</q> dont le contenu
est une séquence de deux octets répétitifs. Cela pourrait être utilisé par un attaquant pour mener
une attaque en déni de service par expression rationnelle (ReDoS) sur un serveur
mono-processus en fournissant un contenu malveillant dans l’en-tête HTTP
<tt>Sec-WebSocket-Extensions</tt>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7663">CVE-2020-7663</a>

<p>Le module websocket-extensions de Ruby avant la version 0.1.5 permet un déni
de service (DoS) à l’aide d’expression rationnelle utilisant la rétro-inspection. 
L’analyseur d’extension pouvait prendre une durée quadratique lors de l’analyse
d’un en-tête contenant une valeur de chaîne de paramètre non <q>fermée</q> dont
le contenu est une séquence de deux octets répétitifs de barre oblique inverse
ou d’un autre caractère. Cela pourrait être utilisé par un attaquant pour mener une attaque en déni
de service par expression rationnelle (ReDoS) sur un serveur mono-processus en
fournissant un contenu malveillant dans l’en-tête HTTP
Sec-WebSocket-Extensions.</p>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.1.2-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-websocket-extensions.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2334.data"
# $Id: $
