#use wml::debian::translation-check translation="c3c5dd384a2a2dac97ef230032c642bf418e89a1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>libplist est une bibliothèque pour lire et écrire les listes binaires et XML
au format d'Apple. Elle fait partie de la pile libimobiledevice, fournissant un
accès aux appareils d’Apple (iPod, iPhone, iPad…).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5209">CVE-2017-5209</a>

<p>La fonction base64decode dans base64.c permet à des attaquants d’obtenir des
informations sensibles de la mémoire du processus ou de causer un déni de
service (lecture excessive de tampon) à l’aide de données de liste de propriétés
d’Apple encodée avec split.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5545">CVE-2017-5545</a>

<p>La fonction main dans plistutil.c permet à des attaquants d’obtenir des
informations sensibles de la mémoire du processus ou de causer un déni de
service (lecture excessive de tampon) à l’aide de données de liste de propriétés
d’Apple trop courtes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5834">CVE-2017-5834</a>

<p>La fonction parse_dict_node dans bplist.c permet à des attaquants de
provoquer un déni de service (lecture hors limites de tas et plantage) à l'aide
 d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5835">CVE-2017-5835</a>

<p>libplist permet à des attaquants de provoquer un déni de service (allocation
de mémoire importante et plantage) à l’aide de vecteurs comportant une taille de
décalage de zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6435">CVE-2017-6435</a>

<p>La fonction parse_string_node dans bplist.c permet à des utilisateurs locaux
de provoquer un déni de service (corruption de mémoire) à l'aide d'un fichier
plist contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6436">CVE-2017-6436</a>

<p>La fonction parse_string_node dans bplist.c permet à des utilisateurs locaux
de provoquer un déni de service (erreur d’allocation mémoire) à l'aide d'un
fichier plist contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6439">CVE-2017-6439</a>

<p>Un dépassement de tampon de tas dans la fonction parse_string_node dans
bplist.c permet à des utilisateurs locaux de provoquer un déni de service
(écriture hors limites) à l'aide d'un fichier plist contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7982">CVE-2017-7982</a>

<p>Un dépassement d'entier dans la fonction plist_from_bin dans bplist.c permet
à des attaquants distants de provoquer un déni de service (lecture hors limites
de tampon de tas et plantage d'application) à l'aide d'un fichier plist
contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.11-3+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libplist.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2168.data"
# $Id: $
