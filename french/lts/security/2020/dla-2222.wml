#use wml::debian::translation-check translation="e517dc276064271b4d1c6fde4ec267d97722d81c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Diverses vulnérabilités mineures ont été corrigées dans libexif, une
bibliothèque pour analyser les fichiers de métadonnées EXIF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20030">CVE-2018-20030</a>

<p>Ce problème a déjà été corrigé par la DLA-2214-1. Cependant, l’amont a fourni
un correctif mis à jour, cela a été suivi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13112">CVE-2020-13112</a>

<p>Plusieurs lectures excessives de tampon dans le traitement de EXIF MakerNote
pouvaient conduire à une divulgation d'informations et des plantages. Ce problème
est différent du
<a href="https://security-tracker.debian.org/tracker/CVE-2020-0093">CVE-2020-0093</a>
déjà réglé.
</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13113">CVE-2020-13113</a>

<p>Une utilisation de mémoire non initialisée dans le traitement de EXIF Makernote
pouvait conduire à des plantages et des conditions d’utilisation de mémoire après
libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13114">CVE-2020-13114</a>

<p>Une taille non limitée dans le traitement de données EXIF MakerNote de Canon
pouvait conduire à des durées de calcul très longues pour le décodage
de données EXIF.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.6.21-2+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libexif.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2222.data"
# $Id: $
