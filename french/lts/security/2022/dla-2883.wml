#use wml::debian::translation-check translation="d03d2bd43b0c5365f8056dbfb5b64ec1bed9169a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait deux problèmes de libération de zone de mémoire dans
uriparser, une bibliothèque C pour analyser des URL d'après le RFC 3986.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46141">CVE-2021-46141</a>

<p>Un problème a été découvert dans les versions d'uriparser avant 0.9.6.
Il réalise des opérations de libération non valables dans uriFreeUriMembers
et uriMakeOwner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46142">CVE-2021-46142</a>

<p>Un problème a été découvert dans les versions d'uriparser avant 0.9.6.
Il réalise des opérations de libération non valables dans
uriNormalizeSyntax.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.8.4-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets uriparser.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2883.data"
# $Id: $
