#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans ClamAV, un moteur d’antivirus au code
source ouvert :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0202">CVE-2018-0202</a>

<p>ClamAV ne traitait pas certains fichiers PDF correctement, en relation avec
un dépassement de tas. Des PDF spécialement contrefaits pourraient faire planter
ClamAV, aboutissant à un déni de service ou éventuellement à une exécution de
code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000085">CVE-2018-1000085</a>

<p>Hanno Böck a découvert que ClamAV ne traitait pas les fichiers XAR
correctement. Des fichiers XAR malformés pourraient faire planter ClamAV à cause
d’une lecture de tas hors limites. Cela pourrait aboutir à un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.99.4+dfsg-1+deb7u1.</p>


<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1307.data"
# $Id: $
