msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:31+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Dată"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Cronologie"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Rezumat"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominări"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Retras"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Dezbatere"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platforme"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Inițiator"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Inițiator propunere A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Inițiator propunere B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Inițiator propunere C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Inițiator propunere D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Inițiator propunere E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Inițiator propunere F"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Inițiator propunere A"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Inițiator propunere A"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Secundare"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Secundare Propunere A "

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Secundare Propunere B "

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Secundare Propunere C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Secundare Propunere D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Secundare Propunere E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Secundare Propunere F"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Secundare Propunere A "

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Secundare Propunere A "

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opoziție"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Text"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Propunerea A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Propunerea B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Propunerea C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Propunerea D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Propunerea E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Propunerea F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Propunerea A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Propunerea A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Alegeri"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Amendament Inițiator"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Secundare Amendament"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Textul amendamentului"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Amendament Inițiator A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Secundare Amendament A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Textul amendamentului A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Amendament Inițiator B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Secundare Amendament B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Textul amendamentului B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Amendament Inițiator C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Secundare Amendament C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Amendament Textul C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Amendamente"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Proceduri"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Necesită vot majoritar"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Date și statistici"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Cvorum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Discuție minimă"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Vot"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Rezultat"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "A&#351;tept&#226;nd sponsorii"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "&#206;n discu&#355;ie"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Vot deschis"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decis"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Retras"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Altele"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Pagina de vot"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Cum&nbsp;s&#259;"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Propune"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Amendeaza propunerea"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Urmărește propunerea"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Citește rezultatul"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Voteaza"
